class Game
  
  $valid_inputs = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]
  def initialize
    @board = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]
    @com = "X" # The computer's marker
    @hum = "O" # The user's marker
    @mode = "PvC" # PvP or PvC
    @computer_dificulty = 1 # Default computer difficulty

  end

  # Clears the terminal for a better UX
  def clr
    system("clear") || system("cls")
  end

  # Prints the board
  def print_board
    clr # Clears screen
    puts " #{@board[0]} | #{@board[1]} | #{@board[2]} \n===+===+===\n #{@board[3]} | #{@board[4]} | #{@board[5]} \n===+===+===\n #{@board[6]} | #{@board[7]} | #{@board[8]} \n"
    # Shows the current player turn
    if @mode == "PvP"
      player = @last_player == "Player 1" ? "Player 2" : "Player 1"
      puts "Make your move #{player} [0-8]:"
    else
      puts "Make your move [0-8]:"
    end
  end

  # Prints a nicer 'game over' message, specifying which player (or computer) won.
  def print_game_over
    clr
    puts '     ____    _    __  __ _____    _____     _______ ____  
    / ___|  / \  |  \/  | ____|  / _ \ \   / / ____|  _ \ 
   | |  _  / _ \ | |\/| |  _|   | | | \ \ / /|  _| | |_) |
   | |_| |/ ___ \| |  | | |___  | |_| |\ V / | |___|  _ < 
    \____/_/   \_\_|  |_|_____|  \___/  \_/  |_____|_| \_\
                                                          '
                    
    if tie(@board)
      puts "It's a tie!" 
    else
      puts "#{@last_player} won!"
    end
  end

  # Configuration for game mode and computer difficulty
  def decide_game
    puts "What mode you wanna play?"
    puts "Player vs Player [1]"
    puts "Player vs Computer [2, any]" # If any other invalid input is provided, the default will be player vs computer
    game_mode = gets.chomp.to_i
    if(game_mode == 1)
      @mode = "PvP" #Player vs Player
    else
      @mode = "PvC" #Player vs Computer
      puts "Please choose a dificulty for the Computer:"
      puts "Easy [1]" 
      puts "Normal [2]" 
      puts "Hard [3, any]"
      pc_diff = gets.chomp.to_i
      if pc_diff == 1
        @computer_dificulty = 3 # Level of depth
      elsif pc_diff == 2
        @computer_dificulty = 5 # Level of depth
      else 
        @computer_dificulty = 8 # Default value for Computer Difficulty (if any other input is provided)
      end
    end
  end

  def start_game
    # first, define the type of game (PvP or PvC) and Computer difficulty (if necessary)
    decide_game
    # start by printing the board
    print_board
    # loop through until the game was won or tied
    until game_is_over(@board) || tie(@board)
      get_human_spot('O') # Specifying which player is making the move
      if !game_is_over(@board) && !tie(@board)
        if(@mode == "PvP") # For Player vs Player
          print_board
          get_human_spot('X')
        end
        if(@mode == "PvC") # For Player vs Computer
          eval_board
        end
      end
      print_board
    end
    print_game_over # Game over screen
  end

  def get_human_spot(player)
    spot = nil
    @hum = player
    @last_player = player == 'O' ? "Player 1" : "Player 2" # Used for correctly showing which player won, if any
    until spot
      spot = gets.chomp
      if($valid_inputs.include?(spot)) # Checking for invalid inputs
        spot = spot.to_i
        if @board[spot] != "X" && @board[spot] != "O" # Checking if the spot is available
          @board[spot] = @hum
        else
          print_board
          puts "Invalid move, this spot is already taken!"
          spot = nil
        end
      else 
        print_board
        puts "Invalid move, please use a digit beetwenn 0 and 8"
        spot = nil
      end
      
    end
  end

  def eval_board
    spot = nil
    until spot
      spot = get_best_move(@board)
      if @board[spot] != "X" && @board[spot] != "O"
        @board[spot] = @com
      else
        spot = nil
      end
    end
    @last_player = "Computer"
  end


  def minimax(_board, depth, maxing) # Function used for generating a value for each possible outcome of a particular game
    board = _board.dup
    if(depth >= @computer_dificulty) # Check for controlling the game difficulty
      return 0 + depth
    end
    if game_is_over(board, 'X') # Computer Won!
      return 100 + depth
    end
    if game_is_over(board, 'O') # Player Won!
      return -100 + depth
    end
    if tie(board) # Both are bad (tied)
      return 0 + depth
    end
    available_spaces = []
    board.each do |s|
      if s != "X" && s != "O"
        available_spaces << s
      end
    end
    if maxing # Maximizing the Computer's paths
      best = -1000000
      available_spaces.each do |as|
        prev = board[as.to_i]
        board[as.to_i] = 'X'
        val = minimax(board, depth+1 , !maxing) # Recaling the method, now doing it for the opposite player
        best = [val, best].max
        board[as.to_i] = prev
      end
      return best
    else #Minimizing the Player's paths
      best = 1000000
      available_spaces.each do |as|
        prev = board[as.to_i]
        board[as.to_i] = 'O'
        val = minimax(board, depth+1 , !maxing) # Recaling the method, now doing it for the opposite player
        best = [val, best].min
        board[as.to_i] = prev
      end
      return best
    end
  end

  # Primary function/method for using the Computer; This method will choose and return the best possible next move for X (Computer) on any given board.
  def get_best_move(_board)
    board = _board.dup # Cloning the board
    bestVal = -1000;
    bestMove = -1;

    available_spaces = []
    board.each do |s|
      if s != "X" && s != "O"
        available_spaces << s
      end
    end

    # For each possible move available, evaluate its results using the minimax algorithm
    available_spaces.each do |as| 
      prev = board[as.to_i]
      board[as.to_i] = 'X'
      val = minimax(board, 0, false) # Receiving the best final result for this particular next move (board[as.to_i])
      if val > bestVal
        bestVal = val
        bestMove = as.to_i
      end
      board[as.to_i] = prev
    end

    return bestMove # Returning the best next move found
  end

  def game_is_over(board, player = nil)
    # Checking if should be tested both players or just one specific player
    if !player 
      return game_is_over(board, "O") || game_is_over(board, "X") 
    end
    return(([board[0], board[1], board[2]].uniq.length == 1 and board[0] == player) ||
    ([board[3], board[4], board[5]].uniq.length == 1 and board[3] == player) ||
    ([board[6], board[7], board[8]].uniq.length == 1 and board[6] == player) ||
    ([board[0], board[3], board[6]].uniq.length == 1 and board[0] == player) ||
    ([board[1], board[4], board[7]].uniq.length == 1 and board[1] == player) ||
    ([board[2], board[5], board[8]].uniq.length == 1 and board[2] == player) ||
    ([board[0], board[4], board[8]].uniq.length == 1 and board[0] == player) ||
    ([board[2], board[4], board[6]].uniq.length == 1 and board[2] == player))
  end

  # Check if the board have tied
  def tie(board)
    board.all? { |spot| spot == "X" || spot == "O" }
  end
  
end

# Init obj
game = Game.new
# Starting the game!
game.start_game